import logging


def get_main_logger():
    logger = logging.Logger('main')
    logger.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    # formatter = logging.Formatter('(message)%s')
    # ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger


if __name__ == "__main__":
    pass
