import unittest
import main
from creds import user_creds


class MainTestCase(unittest.TestCase):
    def test_login(self):
        username = user_creds['username']
        password = user_creds['password']
        inner_html = main.login(username, password)

        self.assertIsNotNone(inner_html)
        self.assertIsInstance(inner_html, str)


if __name__ == "__main__":
    unittest.main()
