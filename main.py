import sys
import re
import argparse
from typing import List, Tuple, Union
from itertools import zip_longest
from bs4 import BeautifulSoup
from result import Result
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
import json
from logger import get_main_logger

_GAP_BETWEEN_SUBJECTS = 7
_DATE_DIRTY_TAG = '\xa0\n\t\t\t\t\t\t\t'
# use the names that are in students web in their numeric format
# so we can use the datetime module
_MONTHS_DICT = {
    'ΣΕΠΤ': 9,
    'ΦΕΒΡ': 2,
    'ΙΟΥΝ': 7
    }

LOGGER = get_main_logger()


def export_json(results_list: List[Result]) -> None:
    json_list = []

    for result in results_list:
        json_list.append(result.as_json())
    # [json_list.append(result.as_json()) for result in results_list]

    with open('results.json', 'w') as json_file:
        json.dump(json_list, json_file, indent=4, ensure_ascii=False)

    LOGGER.info('Done')


def clean_subjects(html_elements_list: List[str]) -> str:
    """
    Get the list with all the html elements and
    extract from it the subjects using regex.
    Use yield so we can return the subject when it
    has been cleaned so we don't need to wait to
    create the whole list
    """
    parentheses_regex = r'\([^()]*\)'
    first_subject_index = 0
    for item in html_elements_list[first_subject_index::_GAP_BETWEEN_SUBJECTS]:
        '''
        get the text from the element and remove
        whitespaces and whatever is in parentheses.
        '''
        yield re.sub(
            parentheses_regex,
            '',
            item.get_text().strip()).strip('\n\t\xa0')


def clean_dates(html_elements_list) -> str:
    first_date_index = 6

    for element in html_elements_list[first_date_index::_GAP_BETWEEN_SUBJECTS]:
        yield element.get_text().replace(_DATE_DIRTY_TAG, '')


def clean_grades(html_elements_list) -> Union[str, None]:
    first_grade_index = 5
    for _ in html_elements_list[first_grade_index::_GAP_BETWEEN_SUBJECTS]:
        # make it integer since the class member is an int
        yield int(_.get_text()) if _.get_text() != '-' else None


def clean_html(dirty_html: str) -> Tuple:
    '''
    Get the "dirty" html and clean it so we can keep the names of the subject,
    the grade and the date
    '''
    subjects = []
    grades = []
    dates = []

    soup = BeautifulSoup(dirty_html, 'html.parser')

    td_elements = soup.find_all('td', class_='topBorderLight')

    c_s = clean_subjects(td_elements)
    c_g = clean_grades(td_elements)
    c_d = clean_dates(td_elements)

    for subject, grade, date in zip(c_s, c_g, c_d):
        subjects.append(subject)
        grades.append(grade)
        dates.append(date)

    return (subjects, grades, dates)


def create_results_list(html_content) -> List[Result]:
    subjects, grades, dates = clean_html(html_content)

    results_list = []

    for name, grade, date_updated in zip_longest(subjects, grades, dates):
        result_grade = int(grade) if grade is not None else None
        results_list.append(Result(name, result_grade, date_updated))

    return results_list


def login(username: str, password: str) -> str:
    '''
    Use selenium to login to the page since requests don't work
    Page is fully served with JS.

    Get the html and pass it to clean_html() so we can clean the tags.
    '''
    login_url = 'https://services.uom.gr/unistudent/login.asp?mnuID=mnu3'
    chromedriver_path = '/usr/bin/chromedriver'
    chrome_options = Options()  # chrome options
    chrome_options.headless = True
    inner_html: str = None

    page_elements = {
            'username_field': 'userName',
            'password_field': 'pwd',
            'submit_login': 'submit1',
            'grades_button': 'mnu3'
        }

    driver = webdriver.Chrome(chromedriver_path, options=chrome_options)
    driver.get(login_url)

    username_field = driver.find_element_by_id(page_elements['username_field'])
    password_field = driver.find_element_by_id(page_elements['password_field'])

    username_field.send_keys(username)
    password_field.send_keys(password)

    driver.find_element_by_id(page_elements['submit_login']).click()  # login

    try:
        driver.find_element_by_id(
            page_elements['grades_button']).click()  # go to grades page
        inner_html = driver.execute_script("return document.body.innerHTML")
    except NoSuchElementException:
        LOGGER.error('login failed')
    finally:
        driver.close()  # ok we are done with selenium

    if inner_html is None:
        sys.exit()

    return inner_html


def fetchGrades(student_id: str, password: str) -> None:
    innerHtml: str = login(student_id, password)
    results: List[Result] = create_results_list(innerHtml)

    export_json(results)


def read_args() -> str:
    parser = argparse.ArgumentParser(description='credentials')
    parser.add_argument('-sid', '--studentid')
    parser.add_argument('-p', '--password')

    args = parser.parse_args()

    s_id, pwd = vars(args).values()

    fetchGrades(s_id, pwd)


if __name__ == "__main__":
    read_args()
