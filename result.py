from dataclasses import dataclass
from datetime import date


@dataclass
class Result(object):
    name: str = None
    grade: int = None
    updated_grade_date: date = None

    def __str__(self) -> str:
        return f'Subject: {self.name} Grade: {self.grade}'

    def as_json(self):
        return {
            'name': self.name,
            'grade': self.grade,
            'grade_update_date': self.updated_grade_date
        }


if __name__ == "__main__":
    pass
